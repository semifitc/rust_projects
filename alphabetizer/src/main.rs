use std::fs::File;
use std::io::{self, BufRead};
use std::io::Write;
use std::path::Path;
use std::env;

fn main() {
    // get name for file
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("Reading File {}", filename);
    
    // File hosts.txt must exist in the current path
    if let Ok(lines) = read_lines(filename) {

        if let Ok(lines_vec) = lines.collect::<Result<Vec<String>, _>>() {
            let ordered_lines = order_strings(lines_vec);

            if let Err(e) = write_to_file("output.txt", ordered_lines) {
                println!("Error writing to file: {}", e);
            }

        } else {

            println!("Error collecting lines");
        }


    } else {

        println!("Error reading lines");
    }

}

// The output is wrapped in a Result to allow matching on errors.
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

// alphabetize the lines
fn order_strings(lines: Vec<String>) -> Vec<String> {
    let mut ordered_lines: Vec<String> = Vec::new();
    let mut current_section = Vec::new();

    for line in lines {
        if line.trim().is_empty() {
            continue;
        }
        if line.starts_with("[") {
            println!("Sorting Category {}", line);

            if !current_section.is_empty() {
                current_section.sort();
                ordered_lines.append(&mut current_section);
                current_section.clear();
            }

            ordered_lines.push(("").to_string());
            ordered_lines.push(line);

        } else {
            current_section.push(line);
        }
    }

    if !current_section.is_empty() {
        current_section.sort();
        ordered_lines.append(&mut current_section);
    }

    return ordered_lines;
}

// write to file
fn write_to_file(filename: &str, lines: Vec<String>) -> io::Result<()> {
    let mut file = File::create(filename).expect("Unable to create file");
    for line in lines {
        writeln!(file, "{}", line).expect("Unable to write to file");
    }

    Ok(())
}
